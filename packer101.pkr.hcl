packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "base_values" {
  assume_role {
    role_arn     = "arn:aws:iam::242392447408:role/OrganizationAccountAccessRole"
    }
  ami_name          = "garth_ami"
  instance_type     = "t2.micro"
  region            = "eu-west-1"
  source_ami        = "ami-0bf84c42e04519c85"
  vpc_id            = "vpc-4bb64132"
  ssh_username      = "ec2-user"
}

build {
  # select source to build ami 
  sources = ["source.amazon-ebs.base_values"]
  # select provisioner to configure a machine
  provisioner "shell" {
    script = "./httpd_simple.sh"
  } 
}