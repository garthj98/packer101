#Installing apache http server
sudo yum -y install httpd
sudo systemctl start httpd
# Starting apache service
# sudo systemctl start http

# sudo sh -c  opens a bash session in sudo - this is a sudo redirect 
# with cat we define the input in the << _END _END_
# Output of cat is contactinated (>) into /var/www/html/index.html
sudo sh -c "cat >/var/www/html/index.html << _END_
<h1>
YELLOWwww 😆
</h1>
<p> this is a paragraph </p>
_END_"